FROM centos:6.6

MAINTAINER Mark Anthony Geronimo <mark_geronimo@rocketmail.com>

#COPY ./run-ssh /usr/local/bin/run-ssh

RUN yum update -y \
	&& yum install tar -y \
	&& curl -o go1.8.3.linux-amd64.tar.gz https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz \
	&& tar -C /usr/local -xzf go1.8.3.linux-amd64.tar.gz \
	&& export PATH=$PATH:/usr/local/go/bin \
	&& mkdir $HOME/go \
	&& mkdir $HOME/go/bin \
	&& mkdir $HOME/go/src \
	&& export GOPATH=$HOME/go \
	&& export PATH=$PATH:$GOPATH/bin \
	&& curl https://glide.sh/get | sh \
	&& chmod -R 777 $GOPATH
	# && chmod +x /usr/local/bin/run-ssh

WORKDIR $HOME/go/src/app